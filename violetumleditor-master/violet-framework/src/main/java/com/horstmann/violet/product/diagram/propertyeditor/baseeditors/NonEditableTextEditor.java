package com.horstmann.violet.product.diagram.propertyeditor.baseeditors;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import com.horstmann.violet.product.diagram.property.text.LineText;
import com.horstmann.violet.product.diagram.property.text.NonEditableText;

public class NonEditableTextEditor extends LineTextEditor {
	protected void setSourceEditor()
    {
        this.source = (NonEditableText) getValue();
    }
    protected LineText getSourceEditor()
    {
        return this.source;
    }
    protected JTextComponent createTextComponent()
    {
    		JTextComponent textField = new JTextField(COLUMNS);
    		//textField.setEnabled(false);
    		textField.setEditable(false);
        return textField;
    }

    private NonEditableText source;
}
