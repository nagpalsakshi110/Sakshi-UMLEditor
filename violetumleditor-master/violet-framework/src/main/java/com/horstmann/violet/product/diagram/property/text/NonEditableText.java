package com.horstmann.violet.product.diagram.property.text;

import com.horstmann.violet.product.diagram.property.text.decorator.OneLineText;

public class NonEditableText extends LineText{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    public NonEditableText()
    {
        super();
        setPadding(5,10);
        oneLineString = new OneLineText();
    }
    public NonEditableText(Converter converter)
    {
        super(converter);
        setPadding(5,10);
        oneLineString = new OneLineText();
    }
    protected NonEditableText(NonEditableText lineText) throws CloneNotSupportedException
    {
        super(lineText);
        oneLineString = lineText.getOneLineString().clone();
    }
    @Override
    public void reconstruction(Converter converter)
    {
        super.reconstruction(converter);
        oneLineString = new OneLineText();
        setPadding(0,10);
        setText(text);
    }

    @Override
    public NonEditableText clone()
    {
        return (NonEditableText)super.clone();
    }

    @Override
    protected NonEditableText copy() throws CloneNotSupportedException
    {
        return new NonEditableText(this);
    }

    /**
     * @see EditableText#setText(String)
     */
    @Override
    final public void setText(String text)
    {
        this.text = text;
        oneLineString = converter.toLineString(this.text);
        setLabelText(toDisplay());
        notifyAboutChange();
    }

    /**
     * @see EditableText#toDisplay()
     */
    @Override
    final public String toDisplay()
    {
        return getOneLineString().toDisplay();
    }

    /**
     * @see EditableText#toEdit()
     */
    @Override
    final public String toEdit()
    {
        return getOneLineString().toEdit();
    }

    /**
     * @see Object#toString()
     */
    @Override
    final public String toString()
    {
        return getOneLineString().toString();
    }

    /**
     * @return one line text
     */
    private OneLineText getOneLineString()
    {
        if(null == oneLineString)
        {
            oneLineString = new OneLineText();
            setPadding(0,10);
            setText(text);
        }
        return oneLineString;
    }

    private String text = "";
    private transient OneLineText oneLineString;


}
