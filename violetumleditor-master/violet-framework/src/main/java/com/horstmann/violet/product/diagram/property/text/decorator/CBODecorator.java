package com.horstmann.violet.product.diagram.property.text.decorator;

/**
 * This class increases text
 *
 * @author Adrian Bobrowski <adrian071993@gmail.com>
 * @date 16.12.2015
 */
public class CBODecorator extends OneLineText
{
	public CBODecorator(OneLineText decoratedOneLineString)
    {
        this.decoratedOneLineString = decoratedOneLineString;
    }

    public CBODecorator clone()
    {
        return new CBODecorator(this.decoratedOneLineString.clone());
    }

    /**
     * @see OneLineText#toDisplay()
     */
    public String toDisplay()
    {
        return "CBO count : " + decoratedOneLineString.toDisplay();
    }
    /**
     * @see OneLineText#toEdit()
     */
    public String toEdit()
    {
        return decoratedOneLineString.toEdit();
    }

    protected OneLineText decoratedOneLineString;
}
